import kotlin.streams.toList

fun high(str: String): String {
    val a = str
        .split(" ")
        .filter(String::isNotEmpty)
        .mapIndexed { i, s -> Triple(s, value(s), i) }
        .stream()
        .sorted { t1, t2 -> if(t2.second != t1.second) t2.second.compareTo(t1.second) else t1.third.compareTo(t2.third) }
        .toList()
    return if (a.isEmpty()) "" else a.first().first
}

private fun value(str: String): Int = str.toCharArray().map { c -> c.toInt() - 'a'.toInt() + 1 }.sum()

fun main() {
    println(high(""))
    println(high("man i need a taxi ixat up to ubud"))
}